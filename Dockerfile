FROM ghcr.io/nginxinc/nginx-unprivileged:stable-alpine

RUN rm -rf /etc/nginx/conf.d
COPY conf.d /etc/nginx/conf.d
